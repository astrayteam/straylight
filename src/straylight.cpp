#include <chrono>
#include <cstdlib>
#include <iostream>

#include "types.h"
#include "parser.h"
#include "kd-tree.h"
#include "camera.h"
#include "trace.h"
#include "ppm.h"
#include "image.h"

namespace {

class TimeDebug {
private:
	std::chrono::high_resolution_clock::time_point timestamp;

public:
	TimeDebug() : timestamp(std::chrono::high_resolution_clock::now()) {}

	inline void print(std::ostream& output, const char* message) {
#ifdef DEBUG
		auto new_timestamp = std::chrono::high_resolution_clock::now();
		std::chrono::duration<float, std::milli> time_diff = new_timestamp - timestamp;
		timestamp = new_timestamp;
		output << "* " << message << " [" << time_diff.count() << "s]" << std::endl;
#endif
	}
};

}

int main(int argc, char* argv[]) {
	if (argc < 3) {
		std::cerr << "usage: " << argv[0]
			<< " width height [ssaa_factor=2] [max_depth=3] [leaf_size=10] [tree_depth=30]" << std::endl;
		return 1;
	}

	int img_width = std::atoi(argv[1]);
	int img_height = std::atoi(argv[2]);
	int img_size = img_width * img_height;


	int ssaa_factor = (argc >= 4) ? std::atoi(argv[3]) : 2;
	int max_depth = (argc >= 5) ? std::atoi(argv[4]) : 3;

	int leaf_size = (argc >= 6) ? std::atoi(argv[5]) : 10;
	int max_kd_tree_depth = (argc >= 7) ? std::atoi(argv[6]) : 30;

	int shot_width = img_width * ssaa_factor;
	int shot_height = img_height * ssaa_factor;
	int shot_size = shot_width * shot_height;

	std::ios_base::sync_with_stdio(false);

	TimeDebug timeDebug;

	parser::config::output_type host_data(parser::config::parse(std::cin));

	Vertex* host_vertices = std::get<0>(host_data).data();
	Vector* host_normals = std::get<1>(host_data).data();
	Triangle* host_triangles = std::get<2>(host_data).data();
	Material* host_materials = std::get<3>(host_data).data();
	Light* host_lights = std::get<4>(host_data).data();

	int vertices_size = std::get<0>(host_data).size();
	int normals_size = std::get<1>(host_data).size();
	int triangles_size = std::get<2>(host_data).size();
	int materials_size = std::get<3>(host_data).size();
	int lights_size = std::get<4>(host_data).size();

	camera::Camera* camera = std::get<6>(host_data);
	Color background = std::get<7>(host_data);
	bool smooth = std::get<8>(host_data);

	timeDebug.print(std::cerr, "parsing input");

	auto tree = kd_tree::build_tree(host_data,leaf_size,max_kd_tree_depth);
	//bfs(tree);

	timeDebug.print(std::cerr, "building kd-tree");

	kd_tree::node* host_nodes = tree.nodes.data();
	int nodes_size = tree.nodes.size();
	int* host_kd_tree_triangles = tree.triangles.data();
	int kd_tree_triangles_size = tree.triangles.size();

	Vertex* device_vertices;
	Vector* device_normals;
	Triangle* device_triangles;
	kd_tree::node* device_nodes;
	int* device_kd_tree_triangles;
	Material* device_materials;
	Light* device_lights;

	cudaCheck(cudaMalloc((void**) &device_vertices, vertices_size * sizeof(Vertex)));
	cudaCheck(cudaMalloc((void**) &device_triangles, triangles_size * sizeof(Triangle)));
	cudaCheck(cudaMalloc((void**) &device_nodes, nodes_size * sizeof(kd_tree::node)));
	cudaCheck(cudaMalloc((void**) &device_kd_tree_triangles, kd_tree_triangles_size * sizeof(int)));
	cudaCheck(cudaMalloc((void**) &device_materials, materials_size * sizeof(Material)));
	cudaCheck(cudaMalloc((void**) &device_lights, lights_size * sizeof(Light)));

	cudaCheck(cudaHostRegister(host_vertices, vertices_size * sizeof(Vertex), cudaHostRegisterPortable));
	cudaCheck(cudaHostRegister(host_triangles, triangles_size * sizeof(Triangle), cudaHostRegisterPortable));
	cudaCheck(cudaHostRegister(host_nodes, nodes_size * sizeof(kd_tree::node), cudaHostRegisterPortable));
	cudaCheck(cudaHostRegister(host_kd_tree_triangles, kd_tree_triangles_size * sizeof(int), cudaHostRegisterPortable));
	cudaCheck(cudaHostRegister(host_materials, materials_size * sizeof(Material), cudaHostRegisterPortable));
	cudaCheck(cudaHostRegister(host_lights, lights_size * sizeof(Light), cudaHostRegisterPortable));

	cudaCheck(cudaMemcpy(device_vertices, host_vertices, vertices_size * sizeof(Vertex), cudaMemcpyHostToDevice));
	cudaCheck(cudaMemcpy(device_triangles, host_triangles, triangles_size * sizeof(Triangle), cudaMemcpyHostToDevice));
	cudaCheck(cudaMemcpy(device_nodes, host_nodes, nodes_size * sizeof(kd_tree::node), cudaMemcpyHostToDevice));
	cudaCheck(cudaMemcpy(device_kd_tree_triangles, host_kd_tree_triangles, kd_tree_triangles_size * sizeof(int), cudaMemcpyHostToDevice));
	cudaCheck(cudaMemcpy(device_materials, host_materials, materials_size * sizeof(Material), cudaMemcpyHostToDevice));
	cudaCheck(cudaMemcpy(device_lights, host_lights, lights_size * sizeof(Light), cudaMemcpyHostToDevice));

	if (smooth && normals_size > 0) {
		cudaCheck(cudaMalloc((void**) &device_normals, normals_size * sizeof(Vector)));
		cudaCheck(cudaHostRegister(host_normals, normals_size * sizeof(Vector), cudaHostRegisterPortable));
		cudaCheck(cudaMemcpy(device_normals, host_normals, normals_size * sizeof(Vector), cudaMemcpyHostToDevice));
	} else {
		device_normals = 0;
	}

	Ray* device_rays;
	cudaCheck(cudaMalloc((void**) &device_rays, shot_size * sizeof(Ray)));

	Color* device_bitmap;
	cudaCheck(cudaMalloc((void**) &device_bitmap, shot_size * sizeof(Color)));

	timeDebug.print(std::cerr, "copying data to gpu");

	camera->shot(device_rays, shot_width, shot_height);

	timeDebug.print(std::cerr, "generating rays");

	trace(shot_width, shot_height,
			device_bitmap,
			device_rays,
			device_nodes,
			nodes_size,
			device_kd_tree_triangles,
			device_triangles,
			device_vertices,
			device_normals,
			device_materials,
			background,
			device_lights,
			lights_size,
			max_depth);

	timeDebug.print(std::cerr, "raytracing");

	image::exposureCorrection(device_bitmap, shot_width, shot_height);
	image::sRGBCorrection(device_bitmap, shot_width, shot_height);

	if (ssaa_factor > 1)
		image::SSAACorrection(device_bitmap, shot_width, shot_height, ssaa_factor);

	timeDebug.print(std::cerr, "image postprocessing");

	Color* host_bitmap;
	host_bitmap = (Color*) malloc(img_size * sizeof(Color));

	cudaCheck(cudaHostRegister(host_bitmap, img_size * sizeof(Color), cudaHostRegisterPortable));
	cudaCheck(cudaMemcpy(host_bitmap, device_bitmap, img_size * sizeof(Color), cudaMemcpyDeviceToHost));

	ppm::write_ppm(std::cout, host_bitmap, img_width, img_height);

	timeDebug.print(std::cerr, "writing image");

	return 0;
}
