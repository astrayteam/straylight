#include <cmath>

#include "transformation.h"

EulerTransformation::EulerTransformation(Vertex begin, FP scale, FP phi, FP theta, FP psi)
	: begin(begin), scale(scale) {
	
	to_matrix[0] = std::cos(psi)*std::cos(phi) - std::cos(theta)*std::sin(phi)*std::sin(psi);
	to_matrix[1] = std::cos(psi)*std::sin(phi) + std::cos(theta)*std::cos(phi)*std::sin(psi);
	to_matrix[2] = std::sin(psi)*std::sin(theta);

	to_matrix[3] = -std::sin(psi)*std::cos(phi) - std::cos(theta)*std::sin(phi)*std::cos(psi);
	to_matrix[4] = -std::sin(psi)*std::sin(phi) + std::cos(theta)*std::cos(phi)*std::cos(psi);
	to_matrix[5] = std::cos(psi)*std::sin(theta);

	to_matrix[6] = std::sin(theta)*std::sin(phi);
	to_matrix[7] = -std::sin(theta)*std::cos(phi);
	to_matrix[8] = std::cos(theta);

	from_matrix[0] = std::cos(phi)*std::cos(psi) - std::cos(theta)*std::sin(phi)*std::sin(psi);
	from_matrix[1] = -std::cos(psi)*std::cos(theta)*std::sin(phi) - std::cos(phi)*std::sin(psi);
	from_matrix[2] = std::sin(phi)*std::sin(theta);

	from_matrix[3] = std::cos(psi)*std::sin(phi) + std::cos(phi)*std::cos(theta)*std::sin(psi);
	from_matrix[4] = std::cos(phi)*std::cos(psi)*std::cos(theta) - std::sin(phi)*std::sin(psi);
	from_matrix[5] = -std::cos(phi)*std::sin(theta);

	from_matrix[6] = std::sin(psi)*std::sin(theta);
	from_matrix[7] = std::cos(psi)*std::sin(theta);
	from_matrix[8] = std::cos(theta);
}

Point EulerTransformation::rotateFrom(const Vertex& vertex) const {
	FP nx = from_matrix[0] * vertex.x;
	nx += from_matrix[1] * vertex.y;
	nx += from_matrix[2] * vertex.z;

	FP ny = from_matrix[3] * vertex.x;
	ny += from_matrix[4] * vertex.y;
	ny += from_matrix[5] * vertex.z;

	FP nz = from_matrix[6] * vertex.x;
	nz += from_matrix[7] * vertex.y;
	nz += from_matrix[8] * vertex.z;

	return Vertex(nx, ny, nz);
}

Point EulerTransformation::rotateTo(const Vertex& vertex) const {
	FP nx = to_matrix[0] * vertex.x;
	nx += to_matrix[1] * vertex.y;
	nx += to_matrix[2] * vertex.z;

	FP ny = to_matrix[3] * vertex.x;
	ny += to_matrix[4] * vertex.y;
	ny += to_matrix[5] * vertex.z;

	FP nz = to_matrix[6] * vertex.x;
	nz += to_matrix[7] * vertex.y;
	nz += to_matrix[8] * vertex.z;

	return Vertex(nx, ny, nz);
}

Point EulerTransformation::from(const Vertex& vertex) const {
	Vertex scaled = scale * vertex;
	return rotateFrom(scaled) + begin;
}

Point EulerTransformation::to(const Vertex& vertex) const {
	return (1/scale)*rotateTo(vertex - begin);
}
