#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include "types.h"

class Transformation {
public:
	virtual ~Transformation() {}
	virtual Vertex from(const Vertex& point) const = 0;
	virtual Vertex to(const Vertex& point) const = 0;
	virtual Vertex rotateFrom(const Vertex& point) const = 0;
	virtual Vertex rotateTo(const Vertex& point) const = 0;
};

class EulerTransformation : public Transformation {
private:
	Vertex begin;
	FP scale;
	FP to_matrix[9];
	FP from_matrix[9];

public:
	// phi from [0, 2*M_PI)
	// theta from [0, PI)
	// psi form [0, 2*M_PI)
	// details: http://mathworld.wolfram.com/EulerAngles.html

	EulerTransformation(Vertex begin, FP scale, FP phi, FP theta, FP psi);
	Point from(const Vertex& point) const;
	Point to(const Vertex& point) const;

	Point rotateFrom(const Vertex& point) const;
	Point rotateTo(const Vertex& point) const;
};

#endif
