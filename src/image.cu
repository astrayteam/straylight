#include "image.h"

namespace image {

namespace {

__device__ FP innerSRGBCorrection(FP value) {
	return (value <= FP(0.0031308)) ? FP(12.92)*value
		: FP(1.055)*powf(value, FP(1.0)/FP(2.4)) - FP(0.055);
}

__device__ FP innerExposureCorrection(FP value, FP exposure) {
	return FP(1)- expf(value * exposure);
}

__global__ void cudaSRGBCorrection(Color* image) {
	const int x = blockIdx.x * IMG_BLOCK_X + threadIdx.x;
	const int y = blockIdx.y * IMG_BLOCK_Y + threadIdx.y;

	int width = gridDim.x * IMG_BLOCK_X;
	int idx = y*width + x;

	width *= ITER_STEP;

	for (int j=0; j<KERNEL_ITERS; ++j) {
		image[idx].red = innerSRGBCorrection(image[idx].red);
		image[idx].green = innerSRGBCorrection(image[idx].green);
		image[idx].blue = innerSRGBCorrection(image[idx].blue);

		idx += width;
	}
}

__global__ void cudaExposureCorrection(Color* image, FP exposure) {
	const int x = blockIdx.x * IMG_BLOCK_X + threadIdx.x;
	const int y = blockIdx.y * IMG_BLOCK_Y + threadIdx.y;

	int width = gridDim.x * IMG_BLOCK_X;
	int idx = y*width + x;

	width *= ITER_STEP;

	for (int j=0; j<KERNEL_ITERS; ++j) {
		image[idx].red = innerExposureCorrection(image[idx].red, exposure);
		image[idx].green = innerExposureCorrection(image[idx].green, exposure);
		image[idx].blue = innerExposureCorrection(image[idx].blue, exposure);

		idx += width;
	}
}

__global__ void cudaSSAACorrection(Color* new_image, Color* image, int factor) {
	const int x = blockIdx.x * IMG_BLOCK_X + factor * threadIdx.x;
	const int y = blockIdx.y * IMG_BLOCK_Y + factor * threadIdx.y;
	const int width = gridDim.x * IMG_BLOCK_X;

	Color new_color(0, 0, 0);

	int idx = width * y + x;
	for (int i=0; i<factor; ++i) {
		for (int j=0; j<factor; ++j)
			new_color += image[idx + j];

		idx += width;
	}

	new_color /= factor * factor;
	new_image[(width/factor) * (blockIdx.y * (IMG_BLOCK_Y/factor) + threadIdx.y)
		+ (blockIdx.x * (IMG_BLOCK_X/factor) + threadIdx.x)] = new_color;
}

}

void sRGBCorrection(
		Color* image,
		int width,
		int height
	)
{
	dim3 dimBlock(IMG_BLOCK_X, ITER_STEP);
	dim3 dimGrid(width/IMG_BLOCK_X, height/IMG_BLOCK_Y);

	cudaSRGBCorrection<<<dimGrid, dimBlock>>>(image);

	cudaCheck(cudaPeekAtLastError());

#ifdef DEBUG
	cudaCheck(cudaDeviceSynchronize());
#endif
}

void exposureCorrection(
		Color* image,
		int width,
		int height,
		FP exposure /*= -1.0*/
	)
{
	dim3 dimBlock(IMG_BLOCK_X, ITER_STEP);
	dim3 dimGrid(width/IMG_BLOCK_X, height/IMG_BLOCK_Y);

	cudaExposureCorrection<<<dimGrid, dimBlock>>>(image, exposure);

	cudaCheck(cudaPeekAtLastError());

#ifdef DEBUG
	cudaCheck(cudaDeviceSynchronize());
#endif
}

void SSAACorrection(
		Color* image,
		int width,
		int height,
		int factor /*= 2*/
	)
{
	int new_width = width/factor;
	int new_height = height/factor;
	int new_size = new_width * new_height;

	Color* new_image;
	cudaCheck(cudaMalloc((void**) &new_image, new_size * sizeof(Color)));

	dim3 dimBlock(IMG_BLOCK_X/factor, IMG_BLOCK_Y/factor);
	dim3 dimGrid(width/IMG_BLOCK_X, height/IMG_BLOCK_Y);

	cudaSSAACorrection<<<dimGrid, dimBlock>>>(new_image, image, factor);

	cudaCheck(cudaPeekAtLastError());

#ifdef DEBUG
	cudaCheck(cudaDeviceSynchronize());
#endif

	cudaCheck(cudaMemcpy(image, new_image, new_size * sizeof(Color), cudaMemcpyDeviceToDevice));
	cudaCheck(cudaDeviceSynchronize());
}

} // namespace
