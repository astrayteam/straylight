#ifndef TYPES_H
#define TYPES_H

#include <cmath>
#include <iostream>

#include "cuda_runtime_api.h"

#define IMG_BLOCK_X 32
#define IMG_BLOCK_Y 32
#define KERNEL_ITERS 16

#define ITER_STEP (IMG_BLOCK_Y/KERNEL_ITERS)

typedef float FP;

const FP eps = 10e-9;

__host__ __device__
inline bool essentiallyEqual(FP lhs, FP rhs) {
	return std::abs(lhs-rhs) <= eps;
}

namespace kd_tree {
    struct voxel {
        FP xl, xr, yl, yr, zl, zr;
        int depth;

        friend std::ostream& operator<<(std::ostream& o, const voxel& V)
        {
            o << V.xl << " " << V.xr << " " << V.yl << " " << V.yr << " " << V.zl << " " << V.zr
                << ", depth: " << V.depth;
            return o;
        }
    };

    struct node {
        voxel V;
        int axis;
        FP plane;
        bool leaf;
        union {
            int left;
            int begin;
        };
        union {
            int right;
            int end;
        };

        __host__ __device__
        node() {}

        node(bool leaf, int axis, FP plane, int begin, int end, voxel V)
            : leaf(leaf), axis(axis), plane(plane), begin(begin), end(end), V(V) {}

        friend std::ostream& operator<<(std::ostream& o, const node& n) {
            o << "axis: " << (n.axis>=0?(n.axis==0?"x":(n.axis==1?"y":"z")):"N/A") << std::endl;
            o << "plane: " << n.plane << std::endl;
            o << "leaf? " << (n.leaf ? "YES" : "NO") << std::endl;
            o << (n.leaf?"begin: ":"left: ") << n.begin << std::endl;
            o << (n.leaf?"end: ":"right: ") << n.end << std::endl;
            o << "voxel: " << n.V;

            return o;
        }

    };

}

struct Vector {
	FP x, y, z;

	__host__ __device__
	Vector(FP x, FP y, FP z)
		: x(x), y(y), z(z)
	{}

    __host__ __device__
	Vector() {}

    __host__ __device__
	FP length() const { return std::sqrt(x*x + y*y + z*z); }

	__host__ __device__
	Vector normalize() const {
		FP len = length();

		if (essentiallyEqual(len, 1))
			return *this;

		return *this/len;
	}

	__host__ __device__
	Vector& operator=(const Vector& rhs) {
		this->x = rhs.x; this->y = rhs.y; this->z = rhs.z;
		return *this;
	}

	__host__ __device__
	Vector& operator+=(const Vector& rhs) {
		x += rhs.x; y += rhs.y; z += rhs.z;
		return *this;
	}

	__host__ __device__
	Vector& operator-=(const Vector& rhs) {
		x -= rhs.x; y -= rhs.y; z -= rhs.z;
		return *this;
	}

	__host__ __device__
	Vector& operator*=(FP rhs) {
		x *= rhs; y *= rhs; z *= rhs;
		return *this;
	}

	__host__ __device__
	Vector& operator/=(FP rhs) {
		x /= rhs; y /= rhs; z /= rhs;
		return *this;
	}

	__host__ __device__
	friend inline Vector operator+(Vector lhs, const Vector& rhs) { return lhs += rhs; }
	__host__ __device__
	friend inline Vector operator-(Vector lhs, const Vector& rhs) { return lhs -= rhs; }
	__host__ __device__
	friend inline Vector operator*(Vector lhs, FP rhs) { return lhs *= rhs; }
	__host__ __device__
	friend inline Vector operator*(FP lhs, Vector rhs) { return rhs *= lhs; }
	__host__ __device__
	friend inline Vector operator/(Vector lhs, FP rhs) { return lhs /= rhs; }
	__host__ __device__
	friend inline Vector operator/(FP lhs, Vector rhs) { return rhs /= lhs; }

    __host__ __device__
    friend inline bool operator==(Vector lhs, Vector rhs)
    {
        return essentiallyEqual(lhs.x,rhs.x)
            && essentiallyEqual(lhs.y,rhs.y)
            && essentiallyEqual(lhs.z,rhs.z);
    }

	friend inline std::ostream& operator<<(std::ostream& output, const Vector& v) {
		output << "(" << v.x << ", " << v.y << ", " << v.z << ")";
		return output;
	}
};

__host__ __device__
inline Vector cross(const Vector& lhs, const Vector& rhs) {
	return Vector(
		lhs.y*rhs.z - lhs.z*rhs.y,
		lhs.z*rhs.x - lhs.x*rhs.z,
		lhs.x*rhs.y - lhs.y*rhs.x);
}

__host__ __device__
inline FP dot(const Vector& lhs, const Vector& rhs)
{
	return lhs.x*rhs.x+lhs.y*rhs.y+lhs.z*rhs.z;
}

typedef Vector Vertex;

typedef Vector Point;

struct Triangle {
	int vertices[3];
	int normals[3];
	int material;

    __host__ __device__
	Triangle() {}

	/*Polygon(decltype(_vertices) vertices, decltype(_normals) normals)
	{
		for (int i=0; i<N; ++i) {
			_vertices[i] = vertices[i];
			_normals[i] = normals[i];
		}
	}*/

	Triangle(int v0, int v1, int v2, int material=0) : material(material)
	{
		int i=0;
		vertices[i++] = v0;
		vertices[i++] = v1;
		vertices[i++] = v2;
	}


	Triangle(int v0, int v1, int v2, int n0, int n1, int n2, int material=0) : material(material)
	{
		int i=0;
		vertices[i++] = v0;
		vertices[i++] = v1;
		vertices[i++] = v2;
		i=0;
		normals[i++] = n0;
		normals[i++] = n1;
		normals[i++] = n2;

	}

	friend std::ostream& operator<<(std::ostream& o, const Triangle& t)
	{
		o << "Triangle(";
		for (int i=0; i<3; ++i) {
			o << t.vertices[i] << "/" << t.normals[i];
			if (i!=2) o << ", ";
		}
		o << ")";
		return o;
	}
};

template <typename T>
void print_collection(std::ostream& o, T begin, T end)
{
	for (T i = begin; i!=end; ++i) {
		o << (*i) << std::endl;
	}
	o << std::endl;
}

struct Ray {
	Point origin;
	Vector direction;

    __host__ __device__
	Ray(Point origin, Vector direction)
		: origin(origin)
	{
		this->direction = direction.normalize();
	}

	Ray() {}

	friend std::ostream& operator<<(std::ostream& output, const Ray& ray) {
		output << "[" << ray.origin << ", " << ray.direction << "]";
		return output;
	}
};

struct Color {
	FP red, green, blue;

	__host__ __device__
	Color(FP red, FP green, FP blue)
		: red(red), green(green), blue(blue)
	{}

    __host__ __device__
	Color() {}


	__host__ __device__
	Color& operator+=(Color rhs)
    {
        red += rhs.red;
        green += rhs.green;
        blue += rhs.blue;
        return *this;
    }

    __host__ __device__
    friend Color operator+(Color lhs, Color rhs)
    {
        return lhs+=rhs;
    }

	__host__ __device__
	Color& operator*=(Color& rhs)
    {
        red *= rhs.red;
        green *= rhs.green;
        blue *= rhs.blue;
        return *this;
    }

    __host__ __device__
    friend Color operator*(Color lhs, Color& rhs)
    {
        return lhs*=rhs;
    }

	__host__ __device__
	Color& operator*=(FP rhs) {
		red *= rhs;
		green *= rhs;
		blue *= rhs;

		return *this;
	}

	__host__ __device__
	Color& operator/=(FP rhs) {
		red /= rhs;
		green /= rhs;
		blue /= rhs;

		return *this;
	}
	
	__host__ __device__
	friend Color operator*(Color lhs, FP rhs)
    {
        return lhs*=rhs;
    }

	__host__ __device__
	friend Color operator/(Color lhs, FP rhs)
    {
        return lhs/=rhs;
    }
};

struct Material {
    Color diffuse, specular;
    FP ambient, reflection, power;
};

struct Light {
    Point position;
    Color color;

    Light(Point position, Color color) : position(position), color(color) {}
};

inline void cudaCheck(cudaError_t status) {
	if (status)
		throw std::exception();
}

#endif // TYPES_H
