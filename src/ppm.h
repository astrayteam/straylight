#ifndef PPM_H
#define PPM_H

#include "types.h"

namespace ppm {

void write_ppm(
		std::ostream& output,
		Color* image, // host
		int width,
		int height,
		int maxcolor = 255
	);

}

#endif
