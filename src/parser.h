#ifndef PARSER_H
#define PARSER_H

#include <istream>
#include <unordered_map>
#include <tuple>
#include <vector>

#include "camera.h"
#include "types.h"

namespace parser {

namespace config {

typedef std::tuple<
	std::vector<Vertex>,              // vertices
	std::vector<Vector>,              // normals
	std::vector<Triangle>,            // faces
	std::vector<Material>,            // materials
	std::vector<Light>,               // lights
	std::pair<Vertex, Vertex>,        // min and max
	camera::Camera*,                  // camera
	Color,                            // scene color
	bool                              // smooth
> output_type;

output_type parse(std::istream& input);

}

namespace obj {

typedef std::tuple<
	std::vector<Vertex>,       // vertices
	std::vector<Vector>,       // normals
	std::vector<Triangle>      // faces
> output_type;

output_type parse(std::istream& input, const std::string& filename);

}

namespace {

inline bool commentFilter(std::string& line) {
	size_t comment = line.find_first_of('#');

	if (comment != std::string::npos)
		line = line.substr(0, comment);

	for (size_t i=0; i<line.size(); ++i) {
		if (!isblank(line.at(i)))
			return true;
	}

	return false;
}

inline void minPerAxis(Vertex& output, const Vertex& input) {
	output.x = std::min(output.x, input.x);
	output.y = std::min(output.y, input.y);
	output.z = std::min(output.z, input.z);
}

inline void maxPerAxis(Vertex& output, const Vertex& input) {
	output.x = std::max(output.x, input.x);
	output.y = std::max(output.y, input.y);
	output.z = std::max(output.z, input.z);
}

inline void findMinMax(std::vector<Vertex>& vertices, Vertex& min, Vertex& max) {
	for (const Vertex& vertex : vertices) {
		minPerAxis(min, vertex);
		maxPerAxis(max, vertex);
	}
}

}

} // namespace parser

#endif
