#include "trace.h"
#include "stdio.h"
#include "math_constants.h"
#include <limits>

struct intersection_result {
	bool intersects;
	Vector point;
};

__device__
intersection_result intersection(Ray ray, Triangle triangle, Vertex* vertices)
{
	intersection_result res;
	res.intersects = false;
	Vector e1,e2,P,Q,T;
	FP det,inv_det,u,v,t;

	e1 = vertices[triangle.vertices[1]]-vertices[triangle.vertices[0]];
	e2 = vertices[triangle.vertices[2]]-vertices[triangle.vertices[0]];

	P = cross(ray.direction,e2);
	det = dot(e1,P);

	if (essentiallyEqual(det,0.0f)) {
		return res;
	}

	inv_det = 1.0f/det;

	T = ray.origin-vertices[triangle.vertices[0]];
	u = dot(T,P)*inv_det;

	if (u<0.0f || u>1.0f) {
		return res;
	}

	Q = cross(T,e1);

	v = dot(ray.direction,Q) * inv_det;

	if(v<0.0f || u+v>1.0f) {
		res.intersects = false;
		return res;
	}

	t = dot(e2,Q) * inv_det;

	if (t>eps) {
		res.intersects = true;
		res.point = ray.origin + t*ray.direction;
	}

	return res;
}

__device__
bool intersects(Ray ray, kd_tree::voxel voxel)
{
	FP a = voxel.xl-ray.origin.x;
	if (a==0) a-=eps;
	FP tmin = a/ray.direction.x;
	a = voxel.xr-ray.origin.x;
	if (a==0) a+=eps;
	FP tmax = a/ray.direction.x;
	if (tmin > tmax) {
		FP temp = tmin;
		tmin = tmax;
		tmax = temp;
	}
	a = voxel.yl-ray.origin.y;
	if (a==0) a-=eps;
	FP tymin = a/ray.direction.y;
	a = voxel.yr-ray.origin.y;
	if (a==0) a+=eps;
	FP tymax = a/ray.direction.y;
	if (tymin > tymax) {
		FP temp = tymin;
		tymin = tymax;
		tymax = temp;
	}
	if ((tmin > tymax) || (tymin > tmax)) {
		return false;
	}
	if (tymin > tmin) {
		tmin = tymin;
	}
	if (tymax < tmax) {
		tmax = tymax;
	}
	a = voxel.zl-ray.origin.z;
	if (a==0) a-=eps;
	FP tzmin = a/ray.direction.z;
	a = voxel.zr-ray.origin.z;
	if (a==0) a+=eps;
	FP tzmax = a/ray.direction.z;
	if (tzmin > tzmax) {
		FP temp = tzmin;
		tzmin = tzmax;
		tzmax = temp;
	}
	if ((tmin > tzmax) || (tzmin > tmax)) {
		return false;
	}
	return true;	
}

__device__
FP distance(Point a, Point b, Point c)
{
	Vector v = a-b;
	Vector u = (c-b).normalize();;
	FP proj = dot(v,u);
	Point d = b+u*proj;
	return (d-a).length();
}


enum {
	PRE_TRAVERSE,
	TRAVERSE,
	LEAF,
	LIGHT,
	END
};

__global__
void trace_kernel(
		Color* bitmap,
		Ray* rays,
		kd_tree::node* kd_tree_nodes,
		int tree_size,
		int* kd_tree_triangles,
		Triangle* triangles,
		Vertex* vertices,
		Vector* normals,
		Material* materials,
		Color background,
		Light* lights,
		int lights_size,
		int max_depth) {
	int x = threadIdx.x*gridDim.x + blockIdx.x;
	int y = threadIdx.y*gridDim.y + blockIdx.y;

	int stack[1024];
	int top;
	int depth = 0;
	FP intensity = 1.0;

	Ray ray = rays[x*gridDim.x*blockDim.x + y];

	kd_tree::node node;
	int triangle, tr_triangle;
	Vector normal, view, tr_normal, tr_view;
	Point intersection_point, tr_intersection_point;

	int leaf_index;
	int light_index;
	FP min_dist;

	int mode = PRE_TRAVERSE;
	bool light = false;
	bool secondary = false;

	Color color(0,0,0);

	while (true) {
		switch (mode) {

			case PRE_TRAVERSE:
				mode = TRAVERSE;
				triangle = -1;
				min_dist = CUDART_INF_F;
				top = 0;
				stack[top] = tree_size-1;
				break;

			case TRAVERSE:
				if (top>=0) {
					node = kd_tree_nodes[stack[top--]];

					if (intersects(ray,node.V)) {
						if (node.leaf) {
							mode = LEAF;
							leaf_index = node.begin;
						} else {
							if (node.right != -1) {
								stack[++top] = node.right;
							}
							if (node.left != -1) {
								stack[++top] = node.left;
							}
						}
					}
				} else {

					if (triangle == -1) {
						color += background * intensity;
						mode = END;
					} else {

						mode = LIGHT;
						if (!light) {
							light_index = 0;
							light = true;
							tr_triangle = triangle;
							tr_view = view*-1.f;
							tr_normal = normal;
							tr_intersection_point = intersection_point;
							if (depth==0) {
								Material m = materials[triangles[triangle].material];
								color += m.diffuse*m.ambient;
							}
						}
					}
				}
				break;

			case LEAF:
				if (leaf_index != node.end) {
					int t = kd_tree_triangles[leaf_index];
					Triangle T = triangles[t];
					intersection_result ir = intersection(ray,T,vertices);

					if (ir.intersects && !((!light) && secondary && (t==tr_triangle))) {
						Vector d = ir.point-ray.origin;
						bool b = true;
						if (d.x*ray.direction.x < 0) b = false;
						if (d.y*ray.direction.y < 0) b = false;
						if (d.z*ray.direction.z < 0) b = false;
						
						if (b) {
						FP dist = d.length();
						if (dist < min_dist) {
							min_dist = dist;
							triangle = t;
							intersection_point = ir.point;
							view = ray.direction;
							int v0 = T.vertices[0];
							int v1 = T.vertices[1];
							int v2 = T.vertices[2];
							Point V0 = vertices[v0];
							Point V1 = vertices[v1];
							Point V2 = vertices[v2];
							if (normals == 0) {
								normal = cross(V1-V0,V2-V0);
							} else {
								Vector n(0,0,0);

								Vector n0 = normals[T.normals[0]];
								Vector n1 = normals[T.normals[1]];
								Vector n2 = normals[T.normals[2]];

								FP d = distance(V0,V1,V2);
								FP dp = distance(ir.point,V1,V2);
								n += n0.normalize() * (dp/d);

								d = distance(V1,V0,V2);
								dp = distance(ir.point,V0,V2);
								n += n1.normalize() * (dp/d);

								d = distance(V2,V0,V1);
								dp = distance(ir.point,V0,V1);
								n += n2.normalize() * (dp/d);

								normal = n;
							}

							normal = normal.normalize();
							if (dot(normal,view*(-1.f)) > 0.f) {
								normal = normal*(-1.f);
							}
						}
						}
					}

					++leaf_index;
				} else {
					mode = TRAVERSE;
				}
				break;

			case LIGHT:
				if (light_index > 0) {
					if (tr_triangle == triangle && (dot(normal,tr_view)<0.f)) {
						Material material = materials[triangles[triangle].material];
						Light l = lights[light_index-1];
						FP lambert = dot(ray.direction,tr_normal);
						color += (l.color * material.diffuse) * lambert * intensity;

						Vector blinnDir = ray.direction - tr_view;
						FP temp = std::sqrt(dot(blinnDir,blinnDir));

						if (!essentiallyEqual(temp,0.f)) {
							blinnDir = blinnDir.normalize();
							FP blinnTerm = dot(blinnDir,tr_normal);

							if (blinnTerm < 0.f) {
								blinnTerm = 0.f;
							}

							blinnTerm = std::pow(blinnTerm,material.power);
							color += l.color * material.specular * blinnTerm * intensity;
						}
					}
				}
				if (light_index != lights_size) {
					mode = PRE_TRAVERSE;
					Light l = lights[light_index];
					ray = Ray(l.position, (intersection_point-l.position));
					++light_index;
				} else {
					++depth;
					if (depth >= max_depth) {
						mode = END;
					} else {
						mode = PRE_TRAVERSE;
						light = false;
						secondary = true;
						FP refl = 2.0f * dot(tr_view*-1.f,tr_normal);
						Vector dir = tr_view*-1.f-tr_normal*refl;
						ray = Ray(tr_intersection_point, dir);
						intensity *= materials[triangles[tr_triangle].material].reflection;
					}
				}
				break;

			case END:
				bitmap[x*gridDim.x*blockDim.x + y] = color;
				return;

			default: 
				return;
		}
	}

}

__host__
void trace(int n, int m,
		Color* bitmap,
		Ray* rays,
		kd_tree::node* kd_tree_nodes,
		int tree_size,
		int* kd_tree_triangles,
		Triangle* triangles,
		Vertex* vertices,
		Vector* normals,
		Material* materials,
		Color background,
		Light* lights,
		int lights_size,
		int max_depth)
{	
	dim3 dimBlock(16,16);
	dim3 dimGrid(n/16,m/16);
	trace_kernel<<<dimGrid,dimBlock>>>(
			bitmap,
			rays,
			kd_tree_nodes,
			tree_size,
			kd_tree_triangles,
			triangles,
			vertices,
			normals,
			materials,
			background,
			lights,
			lights_size,
			max_depth);

	cudaCheck(cudaPeekAtLastError());

#ifdef DEBUG
	cudaCheck(cudaDeviceSynchronize());
#endif
}


