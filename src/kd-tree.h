#ifndef KD_TREE_H
#define KD_TREE_H

#include "types.h"
#include "parser.h"
#include <functional>
#include <queue>
#include <algorithm>

namespace kd_tree {

struct kd_tree {
	std::vector<node> nodes;
	std::vector<int> triangles;
};

typedef std::tuple<int,FP> plane;


inline void bfs(const kd_tree& tree)
{
	std::queue<int> Q;
	Q.push(tree.nodes.size()-1);

	int a = 1;
	int b = 0;
	int nonempty = 0;

	while (!Q.empty()) {


		if (b == a) {
			std::cerr << "-------------" << std::endl << std::endl;
			a *= 2;
			b = 0;
			if (nonempty == 0) {
				return;
			}
			nonempty = 0;
		}

		++b;

		int v = Q.front();
		Q.pop();


		if (v == -1) {
			//std::cout << "EMPTY" << std::endl << std::endl;
			Q.push(-1);
			Q.push(-1);
			continue;
		}

		std::cerr << "===" << v << "===" << std::endl;

		++nonempty;

		auto n = tree.nodes[v];

		std::cerr << n << std::endl;

		if (!n.leaf) {
			Q.push(n.left);
			Q.push(n.right);
		} else {

			std::cerr << "triangles: ";
			for (int i=n.begin; i!=n.end; ++i) {
				std::cerr << tree.triangles[i] << " ";
			}
			std::cerr << std::endl << std::endl;

			Q.push(-1);
			Q.push(-1);
		}

		std::cerr << std::endl;

	}
}

std::vector<Vertex> vertices;
std::vector<Triangle> triangles;

inline bool terminate(int size, const voxel& V, int min_triangles, int max_depth)
{
	return (size <= min_triangles) || (V.depth >= max_depth);
}


inline plane find_plane(const voxel& V)
{
	int axis = V.depth % 3;
	FP plane;
	switch (axis) {
		case 0:
			plane = (V.xl + V.xr)/2;
			break;
		case 1:
			plane = (V.yl + V.yr)/2;
			break;
		case 2:
			plane = (V.zl + V.zr)/2;
	}
	return std::make_tuple(axis,plane);
};



int build_tree(
		kd_tree& tree,
		std::vector<int>& T,
		voxel& V,
		int min_triangles,
		int max_depth
		//std::vector<Vertex> vertices,
		//std::vector<Triangle> triangles)
		)
{

	if (terminate(T.size(),V,min_triangles,max_depth)) {
		auto begin = tree.triangles.size();
		tree.triangles.insert(tree.triangles.end(), T.begin(), T.end());
		auto end = tree.triangles.size();
		tree.nodes.emplace_back(true, -1, 0, begin, end,V);
		return tree.nodes.size()-1;
	}

	int axis;
	FP plane;
	std::tie(axis,plane) = find_plane(V);

	//std::cerr << "axis: " << (axis>=0?(axis==0?"x":(axis==1?"y":"z")):"N/A") << ", plane: " <<  plane << std::endl;

	voxel Vl(V), Vr(V);

	switch (axis) {
		case 0:
			Vl.xr = plane;
			Vr.xl = plane;
			break;
		case 1:
			Vl.yr = plane;
			Vr.yl = plane;
			break;
		case 2:
			Vl.zr = plane;
			Vr.zl = plane;
	}

	Vl.depth++;
	Vr.depth++;

	std::vector<int> Tl, Tr;

	for (auto& ti : T) {
		bool added_l = false, added_r = false;
		Triangle t = triangles[ti];
		for (auto& vi : t.vertices) {
			Vertex v = vertices[vi];

			FP c = 666;
			switch (axis) {
				case 0:
					c = v.x;
					break;
				case 1:
					c = v.y;
					break;
				case 2:
					c = v.z;
			}

			if (c < plane || essentiallyEqual(c,plane)) {
				if (!added_l) {
					Tl.emplace_back(ti);
					added_l = true;
					//std::cerr << ti << ": left" << std::endl;
				}
			}

			if (c > plane || essentiallyEqual(c,plane)) {
				if (!added_r) {
					Tr.emplace_back(ti);
					added_r = true;
					//std::cerr << ti << ": right" << std::endl;
				}
			}
		}

	}

	int nl = -1;
	if (Tl.size() > 0) {
		nl = build_tree(tree, Tl, Vl, min_triangles, max_depth);
	}
	int nr = -1;
	if (Tr.size() > 0) {
		nr = build_tree(tree, Tr, Vr, min_triangles, max_depth);
	}
	tree.nodes.emplace_back(false,axis,plane,nl,nr,V);

	return tree.nodes.size()-1;

}

kd_tree build_tree(parser::config::output_type data, int min_triangles, int max_depth)
{
	kd_tree tree;
	voxel V;
	V.depth = 0;

	const std::pair<Vertex, Vertex>& boundary = std::get<5>(data);

	V.xl = boundary.first.x;
	V.xr = boundary.second.x;
	V.yl = boundary.first.y;
	V.yr = boundary.second.y;
	V.zl = boundary.first.z; 
	V.zr = boundary.second.z;

	std::vector<int> T;
	int s = std::get<2>(data).size();
	for (int i=0; i<s; ++i) {
		T.push_back(i);
	}

	vertices = std::get<0>(data);
	triangles = std::get<2>(data);

	build_tree(tree, T, V, min_triangles, max_depth);

	return tree;
}

}

#endif
