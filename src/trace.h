#ifndef TRACE_H
#define TRACE_H

#include "types.h"

__host__
void trace(int,int,
		Color* bitmap,
		Ray* rays,
		kd_tree::node* kd_tree_nodes,
		int tree_size,
		int* kd_tree_triangles,
		Triangle* triangles,
		Vertex* vertices,
		Vertex* normals,
		Material* materials,
		Color background,
		Light* lights,
		int lights_size,
		int max_depth);

#undef __local__

#endif
