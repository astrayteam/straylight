#ifndef IMAGE_H
#define IMAGE_H

#include "types.h"

namespace image {

void sRGBCorrection(
		Color* image, // device
		int width,
		int height
	);

void exposureCorrection(
		Color* image, // device
		int width,
		int height,
		FP exposure = -1.0
	);

void SSAACorrection(
		Color* image, // device
		int width,
		int height,
		int factor = 2
	);

}

#endif
