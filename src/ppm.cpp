#include "ppm.h"

namespace {

inline void writePixel(
		std::ostream& output,
		const Color& color,
		unsigned int maxcolor
		)
{
	unsigned int r = std::min(static_cast<unsigned int>(color.red * maxcolor), maxcolor);
	unsigned int g = std::min(static_cast<unsigned int>(color.green * maxcolor), maxcolor);
	unsigned int b = std::min(static_cast<unsigned int>(color.blue * maxcolor), maxcolor);

	if (maxcolor <= 255)
		output << static_cast<char>(r) << static_cast<char>(g) << static_cast<char>(b);
	else {
		output << static_cast<char>(r >> 8) << static_cast<char>(r & 255);
		output << static_cast<char>(g >> 8) << static_cast<char>(g & 255);
		output << static_cast<char>(b >> 8) << static_cast<char>(b & 255);
	}
}

}

namespace ppm {

void write_ppm(
		std::ostream& output,
		Color* image, // host
		int width,
		int height,
		int maxcolor /*= 255*/
	)
{
	output << "P6" << std::endl;
	output << width << " " << height << std::endl;
	output << maxcolor << std::endl;

	for (int j=0; j<height; j++) {
		for (int i=0; i<width; i++)
			writePixel(output, image[j*width + i], maxcolor);
	}
}

}
