#include "parser.h"
#include "kd-tree.h"
#include "intersection.h"
#include "trace.h"


int main()
{

	std::vector<Vertex> V;
	std::vector<Vector> N;
	std::vector<Triangle> T;

	V.emplace_back(0,0,0);
	V.emplace_back(1,0,0);
	V.emplace_back(0,1,0);

	V.emplace_back(2,2,0);
	V.emplace_back(4,2,0);
	V.emplace_back(2,4,0);

	T.emplace_back(Triangle(0,1,2));
	T.emplace_back(Triangle(3,4,5));

	parser::output_type o = make_tuple(V,N,T);


	print_collection(std::cout,V.begin(),V.end());
	print_collection(std::cout,T.begin(),T.end());

	auto tree = kd_tree::build_tree<kd_tree::naive_terminate<1,10>,kd_tree::naive_find_plane>(o);

	std::cout << "nodes: " << tree.nodes.size() << std::endl;
	//print_collection(std::cout,tree.nodes.begin(),tree.nodes.end());
	//print_collection(std::cout,tree.triangles.begin(),tree.triangles.end());

	
	//bfs(tree);

	/*kd_tree::voxel v;
	v.xl = 0;
	v.xr = 1;
	v.yl = 0;
	v.yr = 1;
	v.zl = 0;
	v.zr = 1;
	
	bool i = intersection(Ray(Point(.5,1.0001,-1),Vector(0,0,1)),v);

	std::cout << (i?"true":"false") << std::endl;
	*/
	/*if (i.intersects) {
		std::cout << i.point << std::endl;
	}*/

	trace(Ray(Point(2.5,2.5,-1),Vector(0,0,1)),tree.nodes.data(),tree.nodes.size(),tree.triangles.data(),T.data(),V.data());


}

