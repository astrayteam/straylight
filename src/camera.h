#ifndef CAMERA_H
#define CAMERA_H

#include "types.h"

namespace camera {

class Camera {
public:
	virtual ~Camera() {}
	virtual void shot(Ray* rays, int width, int height) = 0;
};

class PerspectiveCamera : public Camera {
private:
	Point _position;
	Vector _direction;
	Vector _sky;
	FP _angle;

public:
	PerspectiveCamera(const Point& position, const Vector& direction,
			const Vector& sky, FP angle);
	void shot(Ray* rays, int width, int height);
};

class OrthographicCamera : public Camera {
private:
	Point _position;
	Vector _direction;
	Vector _sky;
	FP _screen_width;
	FP _screen_height;

public:
	OrthographicCamera(const Point& position, const Vector& direction,
			const Vector& sky, FP screen_width, FP screen_height);
	void shot(Ray* rays, int width, int height);
};

}

#endif // CAMERA_H
