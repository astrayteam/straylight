#include <fstream>
#include <iostream>
#include <limits>
#include <string>
#include <sstream>
#include <stdexcept>
#include <unordered_map>
#include <cmath>

#include "parser.h"
#include "transformation.h"

namespace parser { namespace config {

namespace {

enum components {
	OBJECT, ORTHOGRAPHIC_CAMERA, PERSPECTIVE_CAMERA, LIGHT, SCENE,
};

enum tokens {
	AMBIENT, ANGLE, ANGLE_DEG, COLOR, DIFFUSE, DIRECTION, HEIGHT, POSITION, POWER,
	REFLECTION, ROTATION, ROTATION_DEGREE, SCALE, SKY, SMOOTH, SOURCE, SPECULAR, WIDTH,
};

const std::unordered_map<std::string, components> component_map = {
	{"Object", OBJECT}, {"OrthographicCamera", ORTHOGRAPHIC_CAMERA},
	{"PerspectiveCamera", PERSPECTIVE_CAMERA}, {"Light", LIGHT}, {"Scene", SCENE},
};

const std::unordered_map<std::string, tokens> token_map = {
	{"ambient", AMBIENT}, {"angle", ANGLE}, {"degree-angle", ANGLE_DEG},
	{"color", COLOR}, {"diffuse", DIFFUSE}, {"direction", DIRECTION},
	{"height", HEIGHT}, {"position", POSITION}, {"power", POWER},
	{"reflection", REFLECTION}, {"rotation", ROTATION},
	{"degree-rotation", ROTATION_DEGREE}, {"scale", SCALE}, {"sky", SKY},
	{"smooth", SMOOTH}, {"source", SOURCE}, {"specular", SPECULAR}, {"width", WIDTH},
};

inline void transformVertices(std::vector<Vertex>& vertices,
		const Transformation& transformation)
{
	for (Vertex& v : vertices)
		v = transformation.from(v);
}

inline void transformNormals(std::vector<Vector>& normals,
		const Transformation& transformation)
{
	for (Vector& n : normals)
		n = transformation.rotateFrom(n);
}

inline void transformTriangles(std::vector<Triangle>& triangles,
		int material_idx, int vertices_shift, int normals_shift)
{
	for (Triangle& triangle : triangles) {
		triangle.material = material_idx;

		triangle.vertices[0] += vertices_shift;
		triangle.vertices[1] += vertices_shift;
		triangle.vertices[2] += vertices_shift;

		triangle.normals[0] += normals_shift;
		triangle.normals[1] += normals_shift;
		triangle.normals[2] += normals_shift;
	}
}

inline camera::Camera* parseCamera(std::istream& input, components type, int& line_nr) {
	std::string line, param;

	Point position(0, 0, 0);
	Vector direction(1, 0, 0), sky(0, 0, 1);
	FP angle = M_PI_2;
	FP screen_width = 100;
	FP screen_height = 100;

	while (std::getline(input, line)) {
		++line_nr;

		if(!commentFilter(line))
			continue;

		std::istringstream stream(line);
		if (!(stream >> param))
			continue;

		if (param == "done")
			break;

		try {
			switch (token_map.at(param)) {
				case POSITION:
					stream >> position.x >> position.y >> position.z;
					break;
				case DIRECTION:
					stream >> direction.x >> direction.y >> direction.z;
					break;
				case SKY:
					stream >> sky.x >> sky.y >> sky.z;
					break;
				case ANGLE:
					stream >> angle;
					break;
				case ANGLE_DEG:
					stream >> angle;
					angle *= M_PI/180;
					break;
				case WIDTH:
					stream >> screen_width;
					break;
				case HEIGHT:
					stream >> screen_height;
					break;

				default:
					throw std::out_of_range(line);
			}
		} catch (std::out_of_range) {
			std::cerr << "[config] unrecognized token: " << line
				<< " (" << line_nr << ")" << std::endl;
		}
	}

	if (type == ORTHOGRAPHIC_CAMERA)
		return new camera::OrthographicCamera(position, direction, sky, screen_width, screen_height);

	// default is PerspectiveCamera
	return new camera::PerspectiveCamera(position, direction, sky, angle);
}

inline void parseLight(std::istream& input, std::vector<Light>& lights, int& line_nr) {
	std::string line, param;

	Point position(0, 0, 100);
	Color color(1, 1, 1);

	while (std::getline(input, line)) {
		++line_nr;

		if (!commentFilter(line))
			continue;

		std::istringstream stream(line);
		if (!(stream >> param))
			continue;

		if (param == "done")
			break;

		try {
			switch (token_map.at(param)) {
				case POSITION:
					stream >> position.x >> position.y >> position.z;
					break;
				case COLOR:
					stream >> color.red >> color.green >> color.blue;
					break;

				default:
					throw std::out_of_range(line);
			}
		} catch (std::out_of_range) {
			std::cerr << "[config] unrecognized token: " << line
				<< " (" << line_nr << ")" << std::endl;
		}
	}

	lights.emplace_back(position, color);
}

inline void parseScene(std::istream& input, Color& color, bool& smooth, int& line_nr) {
	std::string line, param, attr;

	smooth = false;
	color = Color(0, 0, 0);

	while (std::getline(input, line)) {
		++line_nr;

		if (!commentFilter(line))
			continue;

		std::istringstream stream(line);
		if (!(stream >> param))
			continue;

		if (param == "done")
			break;

		try {
			switch (token_map.at(param)) {
				case COLOR:
					stream >> color.red >> color.green >> color.blue;
					break;

				case SMOOTH:
					stream >> attr;
					smooth = (attr != "false") && (attr != "0");
					break;

				default:
					throw std::out_of_range(line);
			}
		} catch (std::out_of_range) {
			std::cerr << "[config] unrecognized token: " << line
				<< " (" << line_nr << ")" << std::endl;
		}
	}
}

inline void parseObject(std::istream& input, std::vector<Vertex>& vertices,
		std::vector<Vector>& normals, std::vector<Triangle>& triangles,
		std::vector<Material>& materials, int& line_nr)
{
	std::string line, param;
	std::string filename;

	Material material;
	Point position(0, 0, 0);
	FP phi=0, theta=0, psi=0, scale=1;

	while (std::getline(input, line)) {
		++line_nr;

		if (!commentFilter(line))
			continue;

		std::istringstream stream(line);
		if (!(stream >> param))
			continue;

		if (param == "done")
			break;

		try {
			switch (token_map.at(param)) {
				case POSITION:
					stream >> position.x >> position.y >> position.z;
					break;
				case ROTATION:
					stream >> phi >> theta >> psi;
					break;
				case ROTATION_DEGREE:
					stream >> phi >> theta >> psi;
					phi *= M_PI/180;
					theta *= M_PI/180;
					psi *= M_PI/180;
					break;
				case SCALE:
					stream >> scale;
					break;
				case DIFFUSE:
					stream >> material.diffuse.red >>
						material.diffuse.green >> material.diffuse.blue;
					break;
				case SPECULAR:
					stream >> material.specular.red >>
						material.specular.green >> material.specular.blue;
					break;
				case AMBIENT:
					stream >> material.ambient;
					break;
				case REFLECTION:
					stream >> material.reflection;
					break;
				case POWER:
					stream >> material.power;
					break;
				case SOURCE:
					stream >> filename;
					break;

				default:
					throw std::out_of_range(line);
			}
		} catch (std::out_of_range) {
			std::cerr << "[config] unrecognized token: " << line
				<< " (" << line_nr << ")" << std::endl;
		}
	}

	std::ifstream file(filename);
	if (!file.good()) {
		std::cerr << "[config] can't open file: " << filename << std::endl;
		file.close();

		return;
	}

	obj::output_type objfile = obj::parse(file, filename);
	file.close();

	EulerTransformation transformation(position, scale, phi, theta, psi);
	materials.push_back(material);

	transformVertices(std::get<0>(objfile), transformation);
	transformNormals(std::get<1>(objfile), transformation);
	transformTriangles(std::get<2>(objfile), materials.size()-1, vertices.size(), normals.size());

	vertices.insert(vertices.end(), std::get<0>(objfile).begin(), std::get<0>(objfile).end());
	normals.insert(normals.end(), std::get<1>(objfile).begin(), std::get<1>(objfile).end());
	triangles.insert(triangles.end(), std::get<2>(objfile).begin(), std::get<2>(objfile).end());
}

}

output_type parse(std::istream& input) {
	int line_nr = 0;
	std::string line;

	camera::Camera* camera = nullptr;

	std::vector<Vertex> vertices;
	std::vector<Vector> normals;
	std::vector<Triangle> triangles;
	std::vector<Material> materials;
	std::vector<Light> lights;

	bool smooth;
	Color background;

	while (std::getline(input, line)) {
		++line_nr;

		if (!commentFilter(line))
			continue;

		try {
			components component = component_map.at(line);

			switch (component) {
				case ORTHOGRAPHIC_CAMERA:
				case PERSPECTIVE_CAMERA:
					camera = parseCamera(input, component, line_nr);
					break;

				case OBJECT:
					parseObject(input, vertices, normals,
							triangles, materials, line_nr);
					break;

				case LIGHT:
					parseLight(input, lights, line_nr);
					break;
				
				case SCENE:
					parseScene(input, background, smooth, line_nr);
					break;

				default:
					throw std::out_of_range(line);
			}
		} catch (std::out_of_range) {
			std::cerr << "[config] unsupported token: " << line
				<< " (" << line_nr << ")" << std::endl;
		}
	}

	Vertex vmin(
			std::numeric_limits<FP>::infinity(),
			std::numeric_limits<FP>::infinity(),
			std::numeric_limits<FP>::infinity()
		);

	Vertex vmax(-1*vmin);

	findMinMax(vertices, vmin, vmax);

	return std::make_tuple(vertices, normals, triangles, materials, lights,
			std::make_pair(vmin, vmax), camera, background, smooth);
}

}}
