#include <cmath>

#include "camera.h"
#include "cuda.h"

namespace camera {

namespace {

__global__ void cudaPerspectiveCamera(
		Ray* rays,
		Point position,
		Vector start,
		Vector step_x,
		Vector step_y
	)
{
	const int x = blockIdx.x * IMG_BLOCK_X + threadIdx.x;
	const Vector x_step_x(x*step_x);

	int y = blockIdx.y * IMG_BLOCK_Y + threadIdx.y;
	int width = gridDim.x * IMG_BLOCK_X;
	int idx = y*width + x;

	width *= ITER_STEP;

	for (int j=0; j<KERNEL_ITERS; ++j) {
		rays[idx].origin = position;
		rays[idx].direction = start + x_step_x + y*step_y;

		y += ITER_STEP;
		idx += width;
	}
}

__global__ void cudaOrthographicCamera(
		Ray* rays,
		Vector start,
		Vector step_x,
		Vector step_y,
		Vector direction
	)
{
	const int x = blockIdx.x * IMG_BLOCK_X + threadIdx.x;
	const Vector x_step_x(x*step_x);

	int y = blockIdx.y * IMG_BLOCK_Y + threadIdx.y;
	int width = gridDim.x * IMG_BLOCK_X;
	int idx = y*width + x;

	width *= ITER_STEP;

	for (int j=0; j<KERNEL_ITERS; ++j) {
		rays[idx].origin = start + x_step_x + y*step_y;
		rays[idx].direction = direction;

		y += ITER_STEP;
		idx += width;
	}
}

}

PerspectiveCamera::PerspectiveCamera(const Point& position,
	const Vector& direction, const Vector& sky, FP angle)
	: _position(position), _direction(direction.normalize()),
	_sky(sky.normalize()), _angle(angle)
{}

void PerspectiveCamera::shot(Ray* rays, int width, int height) {
	FP ratio = static_cast<FP>(height)/width;

	FP n_width = 2 * std::tan(_angle/2)/std::sqrt(1+ratio*ratio);
	FP n_height = ratio * n_width;

	Vector right = cross(_direction, _sky);
	Vector down = (-1) * _sky;

	Vector step_x = (n_width/width) * right;
	Vector step_y = (n_height/height) * down;

	Vector start(_direction);
	start -= (n_width/2) * right;
	start -= (n_height/2) * down;
	start += (1.0/2) * step_x;
	start += (1.0/2) * step_y;

	dim3 dimBlock(IMG_BLOCK_X, ITER_STEP);
	dim3 dimGrid(width/IMG_BLOCK_X, height/IMG_BLOCK_Y);

	cudaPerspectiveCamera<<<dimGrid, dimBlock>>>(rays, _position, start, step_x, step_y);

	cudaCheck(cudaPeekAtLastError());

#ifdef DEBUG
	cudaCheck(cudaDeviceSynchronize());
#endif
}

OrthographicCamera::OrthographicCamera(const Point& position,
	const Vector& direction, const Vector& sky, FP screen_width, FP screen_height)
	: _position(position), _direction(direction.normalize()),
	_sky(sky.normalize()), _screen_width(screen_width), _screen_height(screen_height)
{}

void OrthographicCamera::shot(Ray* rays, int width, int height) {
	Vector right = cross(_direction, _sky);
	Vector down = (-1) * _sky;

	Vector step_x = (_screen_width/width) * right;
	Vector step_y = (_screen_height/height) * down;

	Point start = _position;
	start -= (_screen_width/2) * right;
	start -= (_screen_height/2) * down;
	start += (1.0/2) * step_x;
	start += (1.0/2) * step_y;

	dim3 dimBlock(IMG_BLOCK_X, ITER_STEP);
	dim3 dimGrid(width/IMG_BLOCK_X, height/IMG_BLOCK_Y);

	cudaOrthographicCamera<<<dimGrid, dimBlock>>>(rays, start, step_x, step_y, _direction);

	cudaCheck(cudaPeekAtLastError());

#ifdef DEBUG
	cudaCheck(cudaDeviceSynchronize());
#endif
}

} // namespace
