#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>

#include "parser.h"

#define BUFF_SIZE 1024

namespace parser { namespace obj {

namespace {

char buff[BUFF_SIZE];

enum tokens {
	FACE, GROUP, MATERIAL, NORMAL, OBJECT, PARAMETRIZED, SMOOTH, TEXTURE, VERTEX,
};

const std::unordered_map<std::string, tokens> token_map = {
	{"f", FACE}, {"g", GROUP}, {"mtllib", MATERIAL},  {"vn", NORMAL}, {"o", OBJECT},
	{"vp", PARAMETRIZED}, {"s", SMOOTH}, {"vt", TEXTURE}, {"v", VERTEX},
};

inline std::istream& operator>>(std::istream& input, Vector& v) {
	input >> v.x >> v.z >> v.y; // different coordinates
	return input;
}

inline void parseObjVertex(std::istream& input, std::vector<Vertex>& vertices) {
	vertices.emplace_back(0, 0, 0);
	input >> vertices.back();
}

inline void parseObjNormal(std::istream& input, std::vector<Vector>& normals) {
	normals.emplace_back(0, 0, 0);
	input >> normals.back();
}

inline void parseObjFace(std::istream& input, std::vector<Triangle>& faces,
		int vertices_size, int normals_size)
{
	int vertices[3];
	int normals[3];

	std::string token, value;

	for (size_t i=0; i<3; ++i) {
		input >> token;
		std::istringstream token_stream(token);

		getline(token_stream, value, '/');
		std::istringstream(value) >> vertices[i];

		if (vertices[i] < 0)
			vertices[i] += vertices_size;
		else
			--vertices[i];

		if (getline(token_stream, value, '/')
				&& getline(token_stream, value, '/')) {

			std::istringstream(value) >> normals[i];

			if (normals[i] < 0)
				normals[i] += normals_size;
			else
				--normals[i];
		} else
			normals[i] = 0;
			//normals[i] = std::numeric_limits<int>::min();
	}

	faces.emplace_back(
			vertices[0],
			vertices[1],
			vertices[2],
			normals[0],
			normals[1],
			normals[2]
		);
}

}

output_type parse(std::istream& input, const std::string& filename) {
	if (BUFF_SIZE > 0)
		input.rdbuf()->pubsetbuf(buff, BUFF_SIZE);

	int line_nr = 0;

	std::string line;

	std::vector<Vertex> vertices;
	std::vector<Vector> normals;
	std::vector<Triangle> faces;

	while (std::getline(input, line)) {
		++line_nr;

		if (!commentFilter(line))
			continue;

		std::istringstream stream(line);

		std::string token_string;
		if (!(stream >> token_string))
			continue;

		try {
			switch (token_map.at(token_string)) {
				case VERTEX:
					parseObjVertex(stream, vertices);
					break;
				case NORMAL:
					parseObjNormal(stream, normals);
					break;
				case FACE:
					parseObjFace(stream, faces, vertices.size(), normals.size());
					break;

				default:
					std::cerr << "[objparser] unsupported token: " << token_string
						<< " (" << filename << ":" << line_nr << ")" << std::endl;
					break;
			}
		} catch (std::out_of_range) {
			std::cerr << "[objparser] unrecognized token: " << line
				<< " (" << filename << ":" << line_nr << ")" << std::endl;
		}
	}

	return std::make_tuple(vertices, normals, faces);
}

}} // namespace
