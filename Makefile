CODE_DIR = src
TARGET = straylight

.PHONY: $(TARGET) clean

$(TARGET):
	$(MAKE) -C $(CODE_DIR)
	mv $(CODE_DIR)/$(TARGET) $(TARGET)

clean:
	$(MAKE) -C $(CODE_DIR) clean
	rm $(TARGET)
